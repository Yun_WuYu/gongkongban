# QYGLivePlaySDK


## 一、支持平台

SDK支持iOS 8.0以上系统
        
## 二、xcode配置
1. 下载SDK,地址：http://www.artqiyi.com/sdk/gk_sdk_ios/v1.0.0/gk_sdk_ios.zip
2. 将下载下来的QYGLivePlaySDK拷贝至工程目录
3.  使用CocoaPods导入如下库：

``` object-c
    pod 'SocketRocket'
    pod 'TXLiteAVSDK_Player', :podspec => 'http://pod-1252463788.cosgz.myqcloud.com/liteavsdkspec/TXLiteAVSDK_Player.podspec'
```
            
4. 如下地方设置为 no
![](https://gitee.com/uploads/images/2018/0418/171344_e041d2b5_732604.png "1.png")

5. 引用头文件

``` object-c
    #import <QYGLivePlaySDK/QYGLivePlaySDK.h>
```

## 三、代码配置

### 1. 视频播放设置

step 1: 创建播放管理者 同时设置代理及是否是测试环境

``` object-c
    - (QYGLivePlayManager *)playManager{
       if(!_playManager){
           _playManager = [[QYGLivePlayManager alloc] init];
           _playManager.isTest = [QYGKeychain isDollTest];
           _playManager.delegate = self;
           }
       return _playManager;
    }
```    

step 2: 创建正侧面视频要渲染的view

``` object-c    
    // 正面视频渲染的view
    - (UIView *)f_livePlayerView{
        if(!_f_livePlayerView){
            _f_livePlayerView = [[UIView alloc] init];
        }
        return _f_livePlayerView;
    }
    // 侧面视频渲染的view
    - (UIView *)s_livePlayerView{
        if(!_s_livePlayerView){
            _s_livePlayerView = [[UIView alloc] init];
        }
        return _s_livePlayerView;
    }
```

step 3:- (void)viewDidLoad中请求加入房间 并在回调block中开始播放围观视频

``` object-c       
    #pragma  mark -加入房间
    - (void)loginRoom{
        WS(weakSelf);
        [self.playManager loginWithRoomID:self.model.room_id userID:[NSString stringWithFormat:@"%ld",(long)self.userInfo.userId] token:self.userInfo.video_token completeBlock:^(NSError *error) {
            if(!error){
                [weakSelf startRtmp];
            }
        }];
    }
    #pragma  mark 开始播放
    -(BOOL)startRtmp{
        //设置当前视频展示方向
        [self.playManager setFrontRotation:HOME_ORIENTATION_RIGHT];
        [self.playManager setSideRotation:HOME_ORIENTATION_RIGHT];
        
        [self.playManager startPlayFrontWithContainView:self.f_livePlayerView isLive:NO errorBlock:nil];
        [self.playManager startPlaySideWithContainView:self.s_livePlayerView isLive:NO errorBlock:nil];
        return YES;
    }
```       

step 4: 开始游戏后 切换直播流，内部会暂停围观流的播放

``` object-c        
    // 开始游戏
    - (void)readyStartGame{
       // 开始播放直播流
       [self.playManager startPlayFrontWithContainView:self.f_livePlayerView isLive:YES errorBlock:nil];
       [self.playManager startPlaySideWithContainView:self.s_livePlayerView isLive:YES errorBlock:nil];
    }
```       

step 5:游戏结束后切换回围观，内部会停止直播流的播放

``` object-c
    [self.playManager startPlayFrontWithContainView:self.f_livePlayerView isLive:NO errorBlock:nil];
    [self.playManager startPlaySideWithContainView:self.s_livePlayerView isLive:NO errorBlock:nil];
```     

step 6: - (void)dealloc中停止流播放

``` object-c     
    - (void)stopRtmp{
       [self.playManager stopPlaySide];
       [self.playManager stopPlayFront];
    }
```     

视频流回调

``` object-c 
    #pragma  mark 流播放回调
    -(void) onPlayEvent:(int)EvtID withParam:(NSDictionary*)param {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (EvtID == PLAY_EVT_PLAY_BEGIN) {
                self.loadingView.hidden = YES;
            } else{ //if (EvtID == PLAY_EVT_PLAY_LOADING) {
                self.loadingView.hidden = NO;
            }
        });

    } 
```  

###  2.<font color=black size=10>游戏操作设置</font>

step 1: 创建指令管理者

``` object-c        
    - (QYGCommandManager *)commandM{
        if(!_commandM){
            _commandM = [QYGCommandManager instance];
        }
        return _commandM;
    }
```      

step 2: 在游戏开始后设置代理并开启连接

``` object-c 
    self.commandM.delegate = self;
    [self.commandM connectWithUrlStr:@"操作指令链接地址"];
        
``` 

step 3:根据操作指令发送相应消息，例如下：

``` object-c
    - (void)actionButtonDownClick:(NSInteger)act {
      //手指按下, 移动
      NSInteger action = 0;
      if(!self.isSide){
        action = act;
      }else{
        if(act == 3)
        {
            action = ACTION_FORWARD;
          }else if(act == 4){
            action = ACTION_BACK;
          }else if(act == 1){
            action = ACTION_RIGHT;
          }else if(act == 2){
            action = ACTION_LEFT;
          }else{
            action = ACTION_DONE;
          }
      }
      [self catchDoll:action];

    }
    - (void)actionButtonOnClicked:(NSInteger)act{

      NSInteger action = 0;
      if(!self.isSide){
          action = act + 5;
      }else{
          if(act == 3){
            action = ACTION_FORWARD_STOP;
          }else if(act == 4){
            action = ACTION_BACK_STOP;
          }else if(act == 1){
            action = ACTION_RIGHT_STOP;
          }else if(act == 2){
            action = ACTION_LEFT_STOP;
          }
      }
      
      [self catchDoll:action];
    }
    - (void)catchDoll:(NSInteger)action {
      if(action == 5){
          self.isNoCanBack = NO;
          [self.timer invalidate];
          self.timer = nil;
          self.dollBottomView.userInteractionEnabled = NO;
      }
      [self.commandM sendCommand:action];

    }
```

 step 4:游戏结束后断开链接
 
``` object-c
    - (void)showGameResultWithGameResultModel:(NSInteger )result{
    [self.commandM disConnect];
```

 step 5:为防止游戏未结束离开界面，在- (void)dealloc 方法中执行 step 4 并将其设为nill</p>
        


## 四、附加说明

加入房间要用的参数 后台会配置返回


