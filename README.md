## 奇艺果工控板方案娃娃机接入文档

### 时序图
![输入图片说明](https://gitee.com/uploads/images/2018/0315/180105_39ea7f22_1717808.jpeg "工控板对接时序图.jpg")


### 一些准备工作

1. 申请娃娃机管理后台账号密码。
2. 申请接口接入平台id`flatform`、密匙`secret`用于生成签名sign。
3. 申请测试娃娃机。
4. 提供拉流服务器，建立视频推流，添加摄像头到设备管理后台。（网络摄像头为rtmp协议，仅支持app）。

#### 奇艺果后台接口签名算法
生成规则：
- a.声明参数对应的关联数组，然后将数组按key  ASCII编码进行升序排序。
- b.将排序好的数字按key.value拼接成一个字符串，对拼接的字符串进行md5加密
- c.将加密后的得到的字符串再次和约定好的密钥进行拼接，然后再次md5加密，最终得到签名.
参考代码如下：（注：$data为除sign,app,act之外的所有请求参数，含POST、GET参数）
```
   /**
     * 生成sign 算法
     * 
     * @return string
     */
    public function sign(){

        //第三方密钥
        $platform_secret = 'testsecret';

        //请求参数 列表接口
        $params =[
            'platform' => 'platform_id', //平台id
            'page' => 1,
            'per_page' => 20,
            'ts' => time(),
        ];

        //数组排序
        ksort($params);
        $string = '';
        foreach ($params as $key => $value) {
            //字符串链接
            $string .= $key . $value;
        }
        //生成字符串  string = 'page1per_page20platformplatform_idts1518407130';
        $str = md5(md5($string) . $platform_secret);
        return $str;
    }
```
### 奇艺果后台接口目录

- [1.获取娃娃机列表接口](#title-1)
- [2.申请分配娃娃机接口](#title-2)
- [3.操控娃娃机接口](#title-3)
- [4.查询操作结果](#title-4)
- [5.查询设备状态](#title-5)
- [6.查询娃娃商品信息](#title-6)
- [6-1.批量查询娃娃商品信息](#title-6-1)
- [7.创建订单接口](#title-7)
- [8.订单列表](#title-8)
- [9.地址列表](#title-9)
- [10.设置中奖概率](#title-10)
- [11.设置抓力模式](#title-11)
- [12.设置设备游戏时间](#title-12)
- [13.获取设备参数信息](#title-13)
- [14.进入房间创建token](#title-14)
- [15.客户端获取房间信息](#title-15)

### 回调平台接口目录
- [1.回调抓取结果接口](#title-16)
- [2.回调订单结果接口](#title-17)
- [3.通知同步设备状态](#title-18)
- [4.通知同步设备变更](#title-20)

### websort操控娃娃机
- [websort操控娃娃机](#title-19)

<a name="title-1"></a>
##### 获取娃娃机列表接口
- {site_url}/api/index.php?app=doll&act=doll_list
- 请求方法：HTTP GET


请求参数	    |    说明	|        值
---   |  --- |----
platform |	平台方	 |        如：qyguo
page	      |  页码	  |      默认：1
per_page|	每页设备数量	|默认：20


```
应答包：
{
    "done": true, //是否成功
    "msg": "",
    "retval": {
        "list": [{
             "device_id":"",                                             //设备id
             "status": "0",                                              //设备状态
             "name": "设备A0058",                                        //设备名称
             "img": ",                                                  //设备图片
             "stream_address_1": "rtmp:",                               //正面视频地址         
             "stream_address_2": "rtmp:",                               //侧面视频地址
             "stream_address_raw_1": "rtsp:",                           //正面原始视频流
             "stream_address_raw_2": "rtsp:"                            //侧面原始视频流
             "goods_id": "1711209579",                                  //商品ID
             "goods_name": "9寸微笑熊",                                   //商品名称
             "stock": "22",                                              //商品库存
             "room_id" : 1,                                              //房间id
             "device_style" : 3                                          //0:网络摄像头 1:腾讯 2:即构 3:工控板  
         },],
        "per_page": "3",
        "page": 2,
        "total_count": 58
}


```
<a name="title-2"></a>
##### 申请分配娃娃机接口（开始游戏）
- 请求URL：{site_url}/api/index.php?app=doll&act=assign
- 请求方式：GET

请求参数	    |    说明	|        值
---   |  --- |----
device_id	|设备id|	        如：kZFui5UaYHGfkPNBauvq3C
user_id	        |用户id是唯一的|	如：420808
platform	|平台方|	        如：qyguo


```
应答包：  
{
    "done": true,                                                        //Bool true:成功 false:失败
    "msg": "操作成功",                                                    //结果信息
    "retval": {
        "log_id": 51508,                                                 //操作日志id
        "time": 20,                                                      //游戏时间
        "device_id": "ScmAJt8zdiXvXYLHowWpAP",                           //设备id
        "ws_url": "ws://doll.artqiyi.dev:9502?device_id=ScmAJt8zdiXvXYLHowWpAP&user_id=1&platform=qyguo&ts=&sign="
        //  用于websocket方式操作娃娃机                      
    }
}		



```
<a name="title-3"></a>
##### 操控娃娃机接口
- 请求URL：{site_url}/api/index.php?app=doll&act=operate
- 请求方式：GET
- 前置条件：先分配设备且在游戏时间内才可以操作(不用sign校验)


请求参数	     |   说明   |     	值
---   |  --- |----
device_id |	设备id	  |      如：kZFui5UaYHGfkPNBauvq3C
action	     |   娃娃机操作|	1-前进，2-后退，3,-左移，4-右移，5-抓取娃娃 6-前进终止，7-后退终止，8-左移终止，9-右移终止
user_id	      |  用户id是唯一的	|如：420808
platform   |	平台方	      |  如：qyguo


```
应答包：  
{
    "done": true,                                  //Bool true:成功 false:失败
    "msg": "操作成功",                              //
    "retval": []
}

```
<a name="title-4"></a>
##### 查询操作结果
- 请求URL：{site_url}/api/index.php?app=doll&act=operate_result
- 请求方式：GET
备注：(平台有地址管理就不用收货信息 address consignee mobile )


请求参数	    |    说明	 |       值
---   |  --- |----
platform   |	平台方	    |    如：qyguo
log_id	   |     操作记录ID   |	如：23


```
应答包：  
{
    "done": true,                                    //Bool true:成功 false:失败
    "msg": "",              
    "retval": {
        "user_id": "420808",                        //用户id
        "device_id": "AfyThD7bQmqtVayXcCfP2a",      //设备id
        "operate_result": "1",                      //0:失败没有操作1:成功
        "platform": "qyguo",                        //平台ID
        "add_time": "1510142851",                   //时间
        "consignee": "",                            //收货人
        "address": "",                              //地址
        "mobile": "",                               //收货人电话
        "goods_id": "1711022117"                    //商品id
    }
}
```
<a name="title-5"></a>
##### 查询设备状态
- 请求URL：{site_url}/api/index.php?app=doll&act=get_device_status
- 请求方式：GET

请求参数	     |   说明  |	值
---   |  --- |----
platform    |	平台方 |	如：qyguo
device_id   |	设备id |	如：KVA8mV94Vr2R3hrkxK


```
应答包：  
{
    "done": true,
    "msg": "",
    "retval": {
        "device_id": "KVA8mV94Vr2R3hrkxK",                                 //设备id                                 
        "status": "0",                                                     //0:空闲 1:游戏中
        "stream_address_1": "1",                                           //正面视频地址
        "stream_address_2": "1"，                                          //侧面视频地址
        "stream_address_raw_1": "rtsp:",                                   // 正面原始视频流     
        "stream_address_raw_2": "rtsp:"                                    //侧面原始视频流
  
    }
}

```
<a name="title-6"></a>
##### 查询娃娃商品信息
- 请求URL：{site_url}/api/index.php?app=doll&act=get_goods_info
- 请求方式：GET


请求参数	  |      说明 |	值
---   |  --- |----
platform  |	平台方 |	如：qyguo
goods_id  |	商品id |	如：1245312



```
应答包：  
{
    "done": true,
    "msg": "",
    "retval": {
        "goods_id": "1711208844",                                     //娃娃商品id
        "send_type": "0",                                            //发货方式 1：自行发货 2：供应商发货
        "goods_name": "围巾犬",                                        //商品名称
        "default_image": "",                                          //默认图片
        "front_image": "",                                            //正面图片
        "side_image": "",                                             //侧面图片
        "back_image": ""                                              //后面图片
    }
}

```
<a name="title-6-1"></a>
##### 批量查询娃娃商品信息
- 请求URL：{site_url}/api/index.php?app=doll&act=get_goods_infos
- 请求方式：GET


请求参数	  |      说明 |	值
---   |  --- |----
platform  |	平台方 |	如：qyguo
goods_ids  |	商品id,多个id逗号隔开 |	如：1711205201,1711205310



```
应答包：  
{
    "done": true,
    "msg": "",
    "retval": {
	"1711205201": {
		"goods_id": "1711205201",                    //娃娃商品id
		"goods_name": "长条狗",                       //商品名称
		"send_type": "0",                            //发货方式 1：自行发货 2：供应商发货
		"default_image": "",                         //默认图片
		"front_image": "",                           //正面图片
		"side_image": "",                            //侧面图片
		"back_image": ""                             //后面图片
	},
	"1711205310": {
		"goods_id": "1711205310",
		"goods_name": "绿头猫头鹰",
		"send_type": "0",
		"default_image": "",
		"front_image": "",
		"side_image": "",
		"back_image": ""
	}
    }
}

```

<a name="title-7"></a>
##### 创建订单接口
- 请求URL：{site_url}/api/index.php?app=buyer_order&act=create_order
请求方式：POST

请求参数	     |   说明	  |      值
---   |  --- |----
platform|	平台方|	        如：qyguo
user_id	        |用户id是唯一的|	如：420808
username|	用户名	     |   如：13602416937
goods_list|	商品列表|	        json
goods_id	|商品id|	        goods_list下参数
num	        |商品数量|	        goods_list下参数
address	        |地址|	        如：广州天河街道3号
mobile	        |电话|	        如：苏苏苏
consignee	|收货人|	        如：100000

```
应答包：  
{
    "done": true,                                               
    "msg": "创订单成功",
    "retval": [{
        Order_id:201710200478                                   //订单号
     }]
}

```
<a name="title-8"></a>
##### 订单列表
- 请求方式：GET
- `说明：用于平台订单列表内嵌页面（如果平台有开发用户订单管理页面，则不需要）`
- 请求URL：{site_url}/api/index.php?app=buyer_order&act=order_list&platform=qyguo&user_id=420808&page=1
&sign=fe96666d459e71cd089af5c9f29f73d2&ts=1509695453
<a name="title-9"></a>
##### 地址列表
- 请求方式：GET
- `说明：用于平台订单列表内嵌页面（如果平台有开发用户订单管理页面，则不需要）`
- 请求URL：{site_url}/api/index.php?app=my_address&platform=qyguo
&user_id=420808&&sign=fe96666d459e71cd089af5c9f29f73d2&ts=1509695453
<a name="title-10"></a>
##### 设置中奖概率
- 请求URL：{site_url}/api/index.php?app=doll&act=set_winning_probability
- 请求方式：get


请求参数	        |         说明|	        值
---   |  --- |----
platform	 |        平台方|	        如：qyguo
winning_probability	 |中奖概率|	范围: 1~888, 如：100：100次出一次强抓力 数字越大越难中奖
device_id	         |娃娃机设备id|	如：kZFui5UaYHGfkPNBauvq3C

```
应答包：  
{
    "done": true,
    "msg": "操作成功",
    "retval": []
}

```
<a name="title-11"></a>
##### 设置抓力模式
- 请求URL：{site_url}/api/index.php?app=doll&act=set_holding_mode
- 请求方式：get

请求参数	   |     说明	      |  值
---   |  --- |----
platform	|平台方|	        如：qyguo
holding_mode	|抓力设置模式|	1:弱抓力模式2:固定强抓力3:随机强抓力4:固定强抓力-补5:随机强抓力-补
device_id	|娃娃机设备id|	如：kZFui5UaYHGfkPNBauvq3C

```
应答包：  
{
    "done": true,
    "msg": "操作成功",
    "retval": []
}

```
<a name="title-12"></a>
##### 设置设备游戏时间
- 请求URL：{site_url}/api/index.php?app=doll&act=set_playtime
- 请求方式：get

请求参数	         |说明|	        值
---   |  --- |----
platform	 |平台方|	        如：qyguo
playtime         |时间	|        范围: 5~60, 如：30
device_id	 |娃娃机设备id|	如：kZFui5UaYHBauvq3C

```
应答包：  
{
    "done": true,
    "msg": "操作成功",
    "retval": []
}

```
<a name="title-13"></a>
##### 获取设备参数信息

- 请求URL：{site_url}/api/index.php?app=doll&act=get_device_setting
- 请求方式：get


请求参数	       | 说明|	                    值
---   |  --- |----
platform	|平台方|	                    如：qyguo
device_ids	|娃娃机设备id 多个用逗号隔开	|    如：kZFui5UaYHBauvq3C，AfyThD7bQmqtVayXcCfP2a

```
{
    "done": true,
    "msg": "",
    "retval": [{
        "device_id": "AfyThD7bQmqtVayXcCfP2a",                       //设备ID
        "time": 20,                                                  //游戏时间
        "holding_mode":2,                                            //抓力模式
        "winning_probability": 25                                    //中奖概率
    }]
}
```
<a name="title-14"></a>
##### 进入房间创建token

- 请求URL：{site_url}/api/index.php?app=video&act=create_token&user_id=11&expired=60
- 请求方式：get
- 需要权限：加密


请求参数	       | 说明|	   是否必须|                 值
---   |  --- |----|----
user_id| 用户id int  |	 是 |                   如：12313
expired| 过期时间(秒) int 默认半年 |  否 |   如：60



返回结果	       | 说明|              值
---   |  --- |----
token|令牌| 如：12313


```
{
    "done": true,
    "msg": "",
    "retval": {
        "token": "ey1111"
    }
}
```

<a name="title-15"></a>
##### 客户端获取房间信息

- 请求URL：{site_url}/api/index.php?app=video&act=get_room_info&room_id=H046&type=app&user_id=1&token=1
- 请求方式：get
- 需要权限：


请求参数	       | 说明|	   是否必须|                 值
---   |  --- |----|----
room_id|房间id int|	 是 |   如：12313
type| 客户端类型 app:app h5:h5  |  是 |   
user_id|用户id  |  是 |  
token|令牌（进入房间创建token获取）  |  是 |  



返回结果	       | 说明|	    是否必须|                 值
---   |  --- |--- |----
websocket_address_front|前摄像头h5拉流地址|
websocket_address_side|侧摄像头h5拉流地址|
live_pull_address_front|前摄像头rtmp直播拉流地址|	
live_pull_address_side|侧摄像头rtmp直播拉流地址|	
cdn_pull_address_front|前摄像头rtmp旁观拉流地址|	
cdn_pull_address_side|侧摄像头rtmp旁观拉流地址|	
switch_camera_front|前摄像头切换地址(1路推流有效)
switch_camera_side|侧摄像头切换地址（1路推流有效）


```
{
    "done": true,
    "msg": "",
    "retval": {
        "websocket_address_front": "wss://test-live-jsmpeg.artqiyi.com:10005?sign=eyJzaWduIjoiMTk0YTUzOGY4YjgyZmJhOTU5NDdkZDM4MjM3MzY2NjgiLCJ0cyI6MTUyMTAxNzAxMCwicm9vbV9pZCI6IkgwNDYifQ==",
        "websocket_address_side": "wss://test-live-jsmpeg.artqiyi.com:10006?sign=eyJzaWduIjoiMTk0YTUzOGY4YjgyZmJhOTU5NDdkZDM4MjM3MzY2NjgiLCJ0cyI6MTUyMTAxNzAxMCwicm9vbV9pZCI6IkgwNDYifQ==",
        "switch_camera_front": "http://doll.artqiyi.com/api/index.php?app=video&act=camera_switch&device_sn=L159&position=1&ts=1530253549&sign=cf22847e7472774323dd9a6732967&type=app",
        "switch_camera_side": "http://doll.artqiyi.com/api/index.php?app=video&act=camera_switch&device_sn=L159&position=2&ts=1530253549&sign=cf22847e7472774323dd9a6732967&type=app",
        "live_pull_address_front": "rtmp://arqiyit.com/doll/H046_1",
        "live_pull_address_side": "rtmp://arqiyit.com/doll/H046_2",
        "cdn_pull_address_front": "rtmp://cdn.arqiyit.com/doll/H046_1",
        "cdn_pull_address_side": "rtmp://cdn.arqiyit.com/doll/H046_2"
    }
}
```


### 回调接口 （平台提供）
<a name="title-16"></a>
##### 回调抓取结果接口

- 请求方式：GET

请求参数	  |        说明	|        值
---   |  --- |----
platform      |    平台方 |	        如：qyguo
user_id	         | 用户id是唯一的|	如：420808
log_id	         | 操作id	       | 如：1360
address	         | 地址	|        如：广州天河街道3号
consignee        | 收货人	 |       如：苏苏苏
mobile	         | 手机	|        如：136024
goods_id         | 设备商品id	|如：100000
operate_result	 | 操作结果	|1:成功2：失败
sign	          |标识	        |检验url合法用
ts	          |时间戳	|

<a name="title-17"></a>
##### 回调订单结果接口 
- 请求方式：GET

请求参数	  |      说明	|                                值
---   |  --- |----
order_id	|订单ID|	                                如：7432
status	       | 订单状态（30:已发货）|	                如：30
mode	        |物流方式  | 
EXPRESS         |使用物流订单|
NO_EXPRESS     | 无需物流	    |                            如：EXPRESS
shipping_no	|物流单号|	                                如：420679234230
shipping_com|	物流公司标识，用于快递100查询单号的参数|	如：shunfeng
shipping_name|	物流公司名字	                 |       如：“顺丰”
shipping_memo|	发货人备注|	                        如：“已发货”
shipping_time	|发货时间	|                                如：1511079568
sign	        |标识	检验url合法用
ts	        |时间戳


<a name="title-18"></a>
##### 通知同步设备状态
- 请求方式：GET

请求参数	    |    说明|	值
---   |  --- |----
notify_type|	通知类型|	1
device_id|	设备id	|如：adf2sdfeww
push_status|	推流情况|	1：推流失败，2：推流恢复
device_status	|设备情况|	1：机器故障，2：机器恢复
notify_msg	|失败原因|	
sign	|标识	检验url合法用|
ts	|时间戳|	

<a name="title-20"></a>
##### 通知同步设备变更
- 请求方式：GET

请求参数	    |    说明|	值
---   |  --- |----
notify_type|	通知类型|	2
fault_device_id	|故障设备id|	如：adf2sdfeww
new_device_id	|新设备id	
room_id	|房间id	
camera_front|	正面摄像头名字（腾讯云）	
camera_side|	侧面摄像头名字（腾讯云）	
sign|	标识|	检验url合法用
ts	|时间戳	

<a name="title-19"></a>
###  Websocket 娃娃机操作方案

- 机器如果分配成功，接口
{site_url}/api/index.php?app=doll&act=assign&device_id=kZFui5UaYHGfkPNBauvq3C&user_id=420808&platform=qyguo&sign=fe96666d459e71cd089af5c9f29f73d2返回结果中多加一个参数ws_url ，此url对应的是本次游戏发送操作指令的socket连接，连接需要带上用于校验的token，此token为一次性消费推荐令牌桶模式，token与用户关联以支持断线重连
- 例如：ws://testdoll.artqiyi.com:9501?device_id=Z87Ac6pFzeQ96rkoi55ktp&user_id=1&platform=qyguo&ts=1512033962&sign=585640508f202e85bdfaf002b30ed97c



#### 通讯接口
	
###### 1.握手成功
服务端收到建立连接的请求校验成功后，返回这条消息，代表服务端正常，服务器返回	{“type":"Ready"}
###### 2.操作娃娃机
已连接成功，客户端与服务器正常通讯	客户端主动发送	{"type":"Control","data":"1"}	
data：操作指令  1.前进 2.后退 3.向左 4.向右 5.抓取娃娃 6.前进终止 7.后退终止 8.向左终止9.向右终止

###### 3、操作娃娃机结果
客户端发送操作命令后	服务端主动发送	{“type”:”Control_result”,”data":{"command":1}}	
服务端发送，表示操作按键成功


###### 4、抓取结果
客户端发送抓取娃娃命令后，服务端主动发送{“type”:”Result”,”data":{"operate_result":1}}	operate_result：1.成功2.失败


服务端发送游戏结果


###### 5.心跳请求

已连接成功，客户端与服务器正常通讯	客户端主动发送	{“type”:”Delay”}	

游戏过程中的心跳连接，客户端发送


###### 6.心跳响应

已连接成功，客户端与服务器正常通讯	服务端主动发送	{“type”:”Delay_result”,”data”:{“request_time":1512037831.8698}}	


