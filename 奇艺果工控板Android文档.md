奇乐抓娃娃SDK集成指引

1．准备环境

1）	Android Studio2.1或以上

2）	已经下载好 Android SDK 25、Android SDK Build-Tools 25.0.2、Android SDK Platform-Tools 25.*.*

2．下载SDK
	请从[ArtqiyiSDK Android](http://www.artqiyi.com/sdk/gk_sdk_android/v1.0.0/gk_sdk_android.zip)下载SDK

3．集成SDK

3.1 导入sdk

3.1.1解压下载的artqiyisdk包，如artqiyisdk\artqiyi_libs，如图一：

![输入图片说明](https://gitee.com/uploads/images/2018/0420/155559_da52efc1_474729.png "图一.png")
 
图一

3.1.2  添加SDK包。把artqiyisdk.jar放到app/libs下面，如图二：

 ![输入图片说明](https://gitee.com/uploads/images/2018/0420/155620_dea95ae4_474729.png "图二.png")

图二

3.1.3 添加引用。对artqiyisdk.jar右键，选择Add As Library…，如图三：

 ![输入图片说明](https://gitee.com/uploads/images/2018/0420/155634_2d64e58e_474729.png "图三.png")

图三

3.1.4添加so文件。在app/src/main下面建jniLibs文件夹，把解压后的armeabi、armeabi-v7a文件夹复制过来，如图四：

 ![输入图片说明](https://gitee.com/uploads/images/2018/0420/155646_6ac9d797_474729.png "图四.png")

图四

在 app/build.gradle 文件，defaultConfig 节点添加 ndk 节点，指定支持的平台类型添加，内容如下：

        ndk {

            abiFilters 'armeabi' //, 'x86', 'armeabi-v7a', 'x86_64', 'arm64-v8a'

        }

3.1.5 添加权限申明。在app/AndroidManifest.xml 文件，添加如下内容：

    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>

    <uses-permission android:name="android.permission.CAPTURE_AUDIO_OUTPUT"/>

    <uses-permission android:name="android.permission.RECORD_AUDIO"/>

    <uses-permission android:name="android.permission.INTERNET"/>

    <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS" />

    <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />

    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />

    <uses-permission android:name="android.permission.READ_PHONE_STATE" />

    <uses-permission android:name="android.permission.CALL_PHONE"/>

    <uses-permission android:name="android.permission.READ_LOGS" />

    <uses-permission android:name="android.permission.CAMERA" />

    <uses-feature android:name="android.hardware.Camera"/>

    <uses-feature android:name="android.hardware.camera.autofocus" />

4.源码解析

4.1 ArtqiyiPlayManager 播放管理类

1）ArtqiyiPlayManager(Context context):创建实例，需传Context；

2）init():初始化

3) setTestEnv(boolean isTestEnv):设置拉流是否是测试环境的，默认是正式

4）loginRoom(String roomId, String userId, String videoToken, final LoginCallback callback):登入房间，获取视频流，包括正侧面围观和正侧面玩家流，登入成功后，可以调用开始播放的方法

5）setLiveFrontRotation(int rotation):设置玩家模式正面视频角度

6）setLiveSideRotation(int rotation): 设置玩家模式侧面视频角度

7）setOnlookerFrontRotation(int rotation): 设置围观模式正面视频角度

8） setOnlookerSideRotation(int rotation): 设置围观模式侧面视频角度

9）startLivePlayFront(TXCloudVideoView txCloudVideoView, final OnPlayListener listener):开始播放玩家模式正面视频

10）startLivePlaySide(TXCloudVideoView txCloudVideoView, OnPlayListener listener): 开始播放玩家模式侧面视频

11）startOnlookerPlayFront(TXCloudVideoView txCloudVideoView, final OnPlayListener listener): 开始播放围观模式正面视频

12） startOnlookerPlaySide(TXCloudVideoView txCloudVideoView, OnPlayListener listener): 开始播放围观模式侧面视频

13） stopLivePlayFront():停止播放玩家模式正面视频流

14） stopLivePlaySide():停止播放玩家模式侧面视频流

15） stopOnlookerPlayFront():停止播放围观模式正面视频流

16）stopOnlookerPlaySide():停止播放围观模式侧面视频流

17）pauseOnlookerPlayFront():暂停播放围观正面视频流

18）pauseOnlookerPlaySide():暂停播放围观侧面视频流

19）resumeOnlookerPlayFront():继续播放围观正面视频流

20）resumeOnlookerPlaySide():继续播放围观侧面视频流

21） onDestroy():关掉所有流，销毁所有的View，退出时调用

4.2 ArtqiyiCommandManager 命令管理类

1）init():初始化

2）connect(String url, final ConnectCallback callback):连接WebSocket，回调连接结果

3）sendCommand(CommandType commandType):发送命令，CommandType是枚举类，包括各个方向

4）commandStop():停止命令，发送每个命令后，要执行停止命令，不然娃娃机会一直执行该命令

5） disConnect():断开链接

